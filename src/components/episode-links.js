import React from "react"
import "../styles/episode-links.scss"

const EpisodeLinks = (props) => {

    return (
        
        <div className="listen-container episode-links">
          <h4 className = "listen-title"><span role="img" aria-label="Headphones">🎧</span></h4>
          <a className = "listen listen-link" href={props.audio || "/"} target="_blank" rel="noopener noreferrer">Directo</a> 
          <a className = "listen listen-link" href={props.apple || "https://apple.co/3q81FnX"} target="_blank" rel="noopener noreferrer">Apple</a> 
          <a className = "listen listen-link" href={props.google || "https://podcasts.google.com/feed/aHR0cHM6Ly9ydHQuc2hvdy9yc3MueG1s"} target="_blank" rel="noopener noreferrer">Google</a> 
          <a className = "listen listen-link" href={props.spotify || "https://open.spotify.com/show/7nP9rIbGdzQ9WG6vNgASsU"} target="_blank" rel="noopener noreferrer">Spotify</a> 
        </div>
       
    )
  }

  export default EpisodeLinks