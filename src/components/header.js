import { Link } from "gatsby"
import PropTypes from "prop-types"
import React from "react"
import "../styles/header.scss"
import DarkModeToggle from "./dark-mode-toggle"

//import Img from "gatsby-image"
//import { useStaticQuery, graphql } from "gatsby"

const Header = ({ siteTitle, onTransparent }) => {
  return (
    <header>
      <div
        className={"menu-wrapper " + (onTransparent && "header_transparent")}
      >
        <Link to="/" className="link-plain">
          <h1 className="title">Random Tech Talks</h1>
        </Link>
        <Link to="/" className="push">Episodios</Link>
        <span className="menu-slash">/</span>{" "}
        <Link to="/about"> Acerca de </Link>{" "}
          <DarkModeToggle />
      </div>
    </header>
  )
}

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header
