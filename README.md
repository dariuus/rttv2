<img src="static/rtt_big2.png" alt="Random Tech Talks logo" width="200" height="200"  />

# Random Tech Talks

Este es el sitio para el podcast "Random Tech Talks" de [Raúl Merediz](https://twitter/merediz) y [Darío Vasconcelos](https://spaminvoluntario.com). Puedes acceder al sitio [entrando a rtt.show](https://rtt.show).

El sitio está desarrollado en [GatsbyJS](https://www.gatsbyjs.org/), basado en el sitio de otro podcast, "Hear this idea", que puedes encontrar aquí: [hearthisidea.com](https://www.hearthisidea.com).

## 🔍 Detalles

- 🎨 Estilos con [Sass](https://sass-lang.com/) usando [gatsby-plugin-sass](https://www.gatsbyjs.org/packages/gatsby-plugin-sass/).
- ➕ LaTeX parseado con [gatsby-remark-katex](https://www.gatsbyjs.org/packages/gatsby-remark-katex/).
- ✍️ Episodios escritos en [MDX](https://github.com/mdx-js/mdx).
- 💾 Hospedado en [Netlify](https://www.netlify.com/).
- 🧱 Based en [gatsby-starter-default](https://www.gatsbyjs.org/starters/gatsbyjs/gatsby-starter-default/).

Crédito a [Fin Moorhouse](https://www.finmoorhouse.com/) por su [sitio](https://www.hearthisidea.com/)!
[me] and/or the original . Thanks!

Adaptado por [Darío Vasconcelos](https://www.spaminvoluntario.com/).

