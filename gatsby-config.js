module.exports = {
  siteMetadata: {
    title: `Random Tech Talks`,
    description: `Pláticas sobre la influencia de la tecnología en la vida diaria`,
    author: `Raúl Merediz y Darío Vasconcelos`,
    url: `https://www.rtt.show`,
    twitterUsername: `@randomtechtalks`,
    image: "/rttback2.png",
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-sharp`,
    `gatsby-plugin-sass`,
    `gatsby-transformer-sharp`,
    `gatsby-transformer-csv`,
    `gatsby-transformer-json`,
    //`gatsby-transformer-remark`,
    `remark-math`,
    {
      resolve: `gatsby-plugin-mdx`,
      options: {
        remarkPlugins: [ require('remark-math'), require('remark-html-katex') ] ,
        gatsbyRemarkPlugins: [
          {
            resolve: `gatsby-remark-images`,
            options: {
              maxWidth: 960,
              linkImagesToOriginal: false,
              backgroundColor: `transparent`,
              // tracedSVG : true,
            },
          },
          {
            resolve: `gatsby-remark-autolink-headers`,
            options: {
              icon: `
              <svg aria-hidden="true" data-icon="link" height="20" viewBox="0 0 512 512"><path  d="M326.612 185.391c59.747 59.809 58.927 155.698.36 214.59-.11.12-.24.25-.36.37l-67.2 67.2c-59.27 59.27-155.699 59.262-214.96 0-59.27-59.26-59.27-155.7 0-214.96l37.106-37.106c9.84-9.84 26.786-3.3 27.294 10.606.648 17.722 3.826 35.527 9.69 52.721 1.986 5.822.567 12.262-3.783 16.612l-13.087 13.087c-28.026 28.026-28.905 73.66-1.155 101.96 28.024 28.579 74.086 28.749 102.325.51l67.2-67.19c28.191-28.191 28.073-73.757 0-101.83-3.701-3.694-7.429-6.564-10.341-8.569a16.037 16.037 0 0 1-6.947-12.606c-.396-10.567 3.348-21.456 11.698-29.806l21.054-21.055c5.521-5.521 14.182-6.199 20.584-1.731a152.482 152.482 0 0 1 20.522 17.197zM467.547 44.449c-59.261-59.262-155.69-59.27-214.96 0l-67.2 67.2c-.12.12-.25.25-.36.37-58.566 58.892-59.387 154.781.36 214.59a152.454 152.454 0 0 0 20.521 17.196c6.402 4.468 15.064 3.789 20.584-1.731l21.054-21.055c8.35-8.35 12.094-19.239 11.698-29.806a16.037 16.037 0 0 0-6.947-12.606c-2.912-2.005-6.64-4.875-10.341-8.569-28.073-28.073-28.191-73.639 0-101.83l67.2-67.19c28.239-28.239 74.3-28.069 102.325.51 27.75 28.3 26.872 73.934-1.155 101.96l-13.087 13.087c-4.35 4.35-5.769 10.79-3.783 16.612 5.864 17.194 9.042 34.999 9.69 52.721.509 13.906 17.454 20.446 27.294 10.606l37.106-37.106c59.271-59.259 59.271-155.699.001-214.959z"></path></svg>`,
              elements: [`h2`, `h3`],
            },
          },
          `gatsby-remark-copy-linked-files`,
         /* {
            resolve: `gatsby-remark-katex`,
            options: {
              strict: `ignore`,
            },
          }, */
        ],
      },
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Random Tech Talks`,
        short_name: `RTT`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/images/gatsby-icon.png`, // This path is relative to the root of the site.
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `markdown-pages`,
        path: `${__dirname}/src/episodes`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `data`,
        path: `${__dirname}/src/data/`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-transformer-remote-filesystem`,
    {
      resolve: "gatsby-plugin-google-tagmanager",
      options: {
        id: "GTM-KKM6HXS",
        // https://www.gatsbyjs.com/plugins/gatsby-plugin-google-tagmanager/
        // Include GTM in development.
        //
        // Defaults to false meaning GTM will only be loaded in production.
        includeInDevelopment: true,
  
        // datalayer to be set before GTM is loaded
        // should be an object or a function that is executed in the browser
        //
        // Defaults to null
        defaultDataLayer: { platform: "gatsby" },

        // Defaults to false
        enableWebVitalsTracking: true,
      },
    },
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        trackingId: "UA-44315602-3",
        //Defines where to place the tracking script - `true` in the head and `false` in the body
        head: true,
        //  Setting this parameter is optional
        anonymize: true,
        //Setting this parameter is also optional
        respectDNT: true,
      },
    },
    {
      resolve: `gatsby-plugin-goatcounter`,
      options: {
        // Either `code` or `selfHostUrl` is required.
        // REQUIRED IF USING HOSTED GOATCOUNTER! https://[my_code].goatcounter.com
        code: "rtt",
        head: false,
        pixel: true,
      },
    },
    {
      resolve: `mi-gatsby-plugin-podcast-feed-mdx`,
      options: {
        title: `Random Tech Talks`,
        subtitle: `Temas de tecnología para los que saben… y para los que no`,
        description: `Una conversación a veces filosófica, a veces técnica pero siempre divertida sobre temas tecnológicos de todo tipo.`,
        summary: `Únete a nosotros mientras platicamos sobre temas variados, siempre relacionados con la tecnología`,
        podcastType: `episodic`,
        siteUrl: `https://rtt.show`,
        imageUrl: `https://rtt.show/static/rtt_big2.png`,
        feedUrl: `https://rtt.show/rss.xml`,
        language: `es`,
        copyright: `Copyright © 2022 RTT`,
        authorName: `Darío Vasconcelos`,
        ownerName: `Darío Vasconcelos`,
        ownerEmail: `dario@domainmx.mx`,
        managingEditor: `dario@domainmx.mx`,
        webMaster: `dario@domainmx.mx`,
        explicit: `no`,
        publicationDate: `Jan 22, 2020 10:00:00 GMT`,
        category1: `Technology`,
        subCategory1: `Technology`,
        category2: `Education`,
        subCategory2: `How To`,
        category3: `News`,
        subCategory3: `Tech News`,
        timeToLive: `60`,
        outputPath: `/rss.xml`
      },
    },

    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
}
// To add: https://www.youtube.com/watch?v=hbT9gKRMAZU
